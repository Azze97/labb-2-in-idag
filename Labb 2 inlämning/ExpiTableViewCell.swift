//
//  ExpiTableViewCell.swift
//  Labb 2 inlämning
//
//  Created by Aziz Ali on 2018-12-09.
//  Copyright © 2018 Aziz Ali. All rights reserved.
//

import UIKit

class ExpiTableViewCell: UITableViewCell {
    @IBOutlet weak var DescriptionLabel: UILabel!
    @IBOutlet weak var TitleLabel: UILabel!
    @IBOutlet weak var DurationLabel: UILabel!
    @IBOutlet weak var ImageExp: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
