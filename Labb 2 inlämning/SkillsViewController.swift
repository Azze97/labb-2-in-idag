//
//  SkillsViewController.swift
//  Labb 2 inlämning
//
//  Created by Aziz Ali on 2018-12-09.
//  Copyright © 2018 Aziz Ali. All rights reserved.
//

import Foundation
import UIKit

class SkillsViewController : UIViewController {
    
    @IBOutlet weak var bounceImg: UIImageView!
    
    
    @IBAction func btnTouch(_ sender: UIButton) {
        UIView.animate(withDuration: 1, animations:{
            self.bounceImg.frame.origin.y -= 150
        }) {_ in
            UIView.animateKeyframes(withDuration: 1, delay: 0.15, options: [.autoreverse, .repeat], animations: {
                self.bounceImg.frame.origin.y += 150
            })
        }
    }
    
    override func viewDidLoad() {
        
    
    }
    
}

