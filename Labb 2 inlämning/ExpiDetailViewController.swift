//
//  ExpiDetailViewController.swift
//  Labb 2 inlämning
//
//  Created by Aziz Ali on 2018-12-10.
//  Copyright © 2018 Aziz Ali. All rights reserved.
//

import UIKit

class ExpiDetailViewController: UIViewController {
    @IBOutlet weak var ImgExp: UIImageView!
    @IBOutlet weak var DesExp: UILabel!
    @IBOutlet weak var LabelExp: UILabel!
    
    
    var experience = Experience(title: "", description: "", duration: "", image: "")
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ImgExp.image = UIImage(named: experience.image)
        DesExp.text = experience.description
        LabelExp.text = experience.title
    }
    
}

