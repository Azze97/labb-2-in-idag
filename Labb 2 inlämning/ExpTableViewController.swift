//
//  ExperiTableViewController.swift
//  Labb 2 inlämning
//
//  Created by Aziz Ali on 2018-12-09.
//  Copyright © 2018 Aziz Ali. All rights reserved.
//

import UIKit

class ExpTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return experiences.count
        }
        else if section == 1{
            return educations.count
        }
        return 0
    }
    
    var experiences = [Experience(title: "Det du skriver här, hamnar här. Råkade byta plats på descripton och title, så titlen är description och description är titeln, men det gör inget.", description: "Ditt Jobb", duration: "2018-2018", image: "work")]
    var educations = [Experience(title: "Det du skriver här, hamnar här så skriv lite om företaget/skolan här", description: "Jönköping University", duration: "2016-2019", image: "student")]
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ExpiTableViewCell", for: indexPath) as? ExpiTableViewCell {
            if indexPath.section == 0 {
                cell.DescriptionLabel.text = experiences[indexPath.row].description
                cell.DurationLabel.text = experiences[indexPath.row].duration
                cell.TitleLabel.text = experiences[indexPath.row].title
                cell.ImageExp.image = UIImage(named: experiences[indexPath.row].image)
                return cell
            }
            else if indexPath.section == 1 {
                cell.DescriptionLabel.text = educations[indexPath.row].description
                cell.DurationLabel.text = educations[indexPath.row].duration
                cell.TitleLabel.text = educations[indexPath.row].title
                cell.ImageExp.image = UIImage(named: educations[indexPath.row].image)
                return cell
            }
            else {
                return UITableViewCell()
            }
        }
        else {
            return UITableViewCell()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Work"
        }
        else if section == 1{
            return "Education"
        }
        else {
            return "ERROR"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "ExperienceTouch", sender: indexPath)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ExpiDetailViewController {
            if let indexPath = sender as? IndexPath {
                var experience: Experience
                if indexPath.section == 0 {
                    experience = experiences[indexPath.row]
                }
                else {
                    experience = educations[indexPath.row]
                }
                destination.experience = experience
            }
        }
    }
    
    
    
}
